# Login Page Elements
txt_UserName = "name=username"
txt_Password = "name=password"
btn_Login = "xpath=//button[contains(text(), 'Connexion')]"


# Dashboard Page Elements
lbl_Title = "xpath=//h1[@id='HEADER_TITLE']/span"
link_HeaderUserMenu = "id=HEADER_USER_MENU_POPUP_text"
link_HeaderDeconnexion = "id=HEADER_USER_MENU_LOGOUT_text"

# Dashbord Config page elements
img_RoueDentee = "id=HEADER_TITLE_MENU"
btn_ChangerDisposition = "id=template_x002e_customise-layout_x002e_customise-user-dashboard_x0023_default-change-button-button"
btn_Mode = "id=template_x002e_customise-layout_x002e_customise-user-dashboard_x0023_default-select-button-dashboard"
btn_ChangerDisposition_OK = "id=template_x002e_customise-dashlets_x002e_customise-user-dashboard_x0023_default-save-button-button"
